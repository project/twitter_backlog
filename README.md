# Twitter Backlog

This module allows users to import the backlog of their Twitter status
messages, prior to the 20 messages imported by the
[Twitter](https://www.drupal.org/project/twitter) module by default.

## Dependencies

* [Twitter](https://www.drupal.org/project/twitter)

## Compatibility

The 7.x-1.x branch of Twitter Backlog is intended to work with the 7.x-5.x
branch of the Twitter module. The 7.x-2.x branch of the Twitter Backlog module
is intended to work with the 7.x-6.x branch of the Twitter module.

## Installation

1. If not already installed, download and install the dependencies above.
2. Download the Twitter Backlog module and follow the instructions for
   [installing contributed modules](http://drupal.org/node/895232).

## Usage

1. Configure the Twitter module as outlined in that module's
   [documentation](https://www.drupal.org/node/430658).
2. Once you have added a Twitter account to your site, go to the user's
   **Edit** page (user/N/edit) and then to the **Twitter accounts** sub-tab
   (user/[user id]/edit/twitter). Make sure that you have checked the
   **Tweets** checkbox and then click on the **Save changes** button.
3. Click on the **import backlog** link.
